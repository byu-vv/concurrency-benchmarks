﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TwoStage
{
    public class ReadThread
    {
        TwoStage ts;
        Thread readThread;

        public ReadThread(TwoStage ts)
        {
            this.ts = ts;
            readThread = new Thread(new ThreadStart(run));
            readThread.Start();
        }

        public void run()
        {
            ts.B();
        }

        public void joinTheThread()
        {
            readThread.Join();
        }

    }
}
