﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TwoStage
{
    public class TwoStageThread
    {
        TwoStage ts;
        Thread twoStageThread;

        public TwoStageThread(TwoStage ts)
        {
            this.ts = ts;
            twoStageThread = new Thread(new ThreadStart(run));
            twoStageThread.Start();
        }

        public void run()
        {
            ts.A();
        }

        public void joinTheThread()
        {
            twoStageThread.Join();
        }
    }
}
