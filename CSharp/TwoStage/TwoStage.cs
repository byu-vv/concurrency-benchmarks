﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TwoStage
{
    public class TwoStage
    {
        public Data data1, data2;

        public TwoStage(Data data1, Data data2)
        {
            this.data1 = data1;
            this.data2 = data2;
        }

        public void A()
        {
            lock (data1)
            {
                data1.value = 1;
            }

            lock (data2)
            {
                data2.value = data1.value + 1;
            }
        }

        public void B()
        {
            int t1 = -1, t2 = -1;
            lock (data1)
            {
                if (data1.value == 0) return;
                t1 = data1.value;
            }

            lock (data2)
            {
                t2 = data2.value;
            }

            if (t2 != t1 + 1)
            {
                Console.Write("Found the violation");
                Environment.Exit(1);
            }
        }
    }
}
