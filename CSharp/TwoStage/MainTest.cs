﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
namespace TwoStage
{
   
    
    public class ChessTest
    {
        public static void Main(String[] args)
        {
            Console.WriteLine(Run());
        }
        
        public static Boolean Run()
        {
            int iTthreads = 10;
            int iRthreads = 1;

            TwoStage ts;
            Data data1, data2;

            data1 = new Data();
            data2 = new Data();

            ts = new TwoStage(data1, data2);

            TwoStageThread[] tst = new TwoStageThread[iTthreads];
            ReadThread[] rt = new ReadThread[iRthreads];

            for (int i = 0; i < iTthreads; i++)
            {
                tst[i] = new TwoStageThread(ts);
            }

            for (int i = 0; i < iRthreads; i++)
            {
                rt[i] = new ReadThread(ts);
            }

            for (int i = 0; i < iTthreads; i++)
            {
                tst[i].joinTheThread();
            }

            for (int i = 0; i < iRthreads; i++)
            {
                rt[i].joinTheThread();
            }
            return true;
        }
    }
}
