﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace accountsubtype
{
   
    public class ChessTest
    {

        public static void Main(String[] args)
        {
            Console.WriteLine(Run());
        }
        
        public static Boolean Run() 
        {
            int numBusinessAccounts = 8;
            int numPersonalAccounts = 1;

            Bank bank = new Bank(numBusinessAccounts, numPersonalAccounts, 100);

            for (int i = 0; i < numBusinessAccounts + numPersonalAccounts; i++)
            {
                if (bank.getAccount(i).getBalance() != 300)
                {
                    Console.WriteLine("Bug Found");
                    return false;
                }
            }
            return true;
        }
    }
}
