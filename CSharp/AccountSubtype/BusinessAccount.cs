﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accountsubtype
{
    public class BusinessAccount : Account
    {
        public BusinessAccount(int number, int amt) : base (number, amt)
        {
        }

        public override void transfer(Account dest, int transferAmount)
        {
            lock (this)
            {
                amount -= transferAmount;
                lock (dest)
                {
                    dest.amount += transferAmount;
                }
            }
        }
    }
}
