﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accountsubtype
{
    public class Bank
    {
        Manager[] managers;
        Account[] accounts;

        public Bank(int numBusinessAccounts, int numPersonalAccounts,
            int initialBalance)
        {
            accounts = new Account[numBusinessAccounts + numPersonalAccounts];
            int i;
            for (i = 0; i < numBusinessAccounts; i++)
            {
                accounts[i] = new BusinessAccount(i, initialBalance);
            }
            for (; i < numBusinessAccounts + numPersonalAccounts; i++)
            {
                accounts[i] = new PersonalAccount(i, initialBalance);
            }

            managers = new Manager[numBusinessAccounts + numPersonalAccounts];
            for (i = 0; i < managers.Length; i++)
            {
                managers[i] = new Manager(this, accounts[i], i);
            }

            for (i = 0; i < managers.Length; i++)
            {
                managers[i].joinTheThread();
            }
        }



        public Account getAccount(int number) 
        {
          return accounts[number];
        }

        public int nextAccountNumber(int number)
        {
            return (number + 1) % accounts.Length;
        }
    }
}
