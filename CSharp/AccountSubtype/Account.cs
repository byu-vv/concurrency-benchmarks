﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accountsubtype
{
    abstract public class Account
    {
        protected String name;
        protected int number;
        volatile public int amount;

        public Account(int number, int initBalance)
        {
            this.name = "Account" + number;
            this.number = number;
            this.amount = initBalance;
        }

        public String getName()
        {
            lock (this)
            {
                return name;
            }
        }

        public int getBalance()
        {
            lock (this)
            {
                return amount;
            }
        }

        public void depoist(int money)
        {
            lock (this)
            {
                amount += money;
            }
        }

        public void withdraw(int money)
        {
            lock (this)
            {
                amount -= money;
            }
        }

        abstract public void transfer(Account dest, int mn);
    }
}
