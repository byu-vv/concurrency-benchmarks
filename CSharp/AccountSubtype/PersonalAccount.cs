﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accountsubtype
{
    class PersonalAccount : Account
    {
        public PersonalAccount(int number, int initialBalance) : base (number, initialBalance)
        {
        }

        public override void transfer(Account ac, int mn)
        {
            lock (this)
            {
                amount -= mn;
                ac.amount += mn;
            }
        }
    }
}
