﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace accountsubtype
{
    
    public class Manager
    {
        Bank bank;
        Account account;
        int accountNumber;
        Thread manager;

        public Manager(Bank b, Account a, int n)
        {
            bank = b;
            account = a;
            accountNumber = n;
            manager = new Thread(new ThreadStart(run));
            manager.Start();
        }

        public void joinTheThread()
        {
            manager.Join();
        }

        public void run()
        {
            int nextNumber;

            account.depoist(300);
            nextNumber = bank.nextAccountNumber(accountNumber);
            account.transfer(bank.getAccount(nextNumber), 10);

            account.depoist(10);
            account.withdraw(20);
            account.depoist(10);
            account.transfer(bank.getAccount(nextNumber), 10);

            account.withdraw(100);
            
        }
    }
}
