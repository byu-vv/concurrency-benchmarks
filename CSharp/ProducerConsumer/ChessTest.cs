﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Runtime.CompilerServices;


namespace ProducerConsumer
{
        

    public class ChessTest
    {

        public static int PRODS = 1;
        public static int CONS = 8;
        public static int COUNT = 16;
        public static int total = 0;       


        public static void Main(String[] args)
        {
            Console.WriteLine(Run());
        }

     
        public static bool Run()
        {
            Consumer[] cons = new Consumer[CONS];
            Buffer b = new Buffer(5);
            Producer[] prods = new Producer[PRODS];

            for (int i = 0; i < PRODS; i++)
                prods[i] = new Producer(b);

            for (int i = 0; i < CONS; i++)
                cons[i] = new Consumer(b);

            for (int i = 0; i < CONS; i++)
                 cons[i].JoinThread();

      
            if (total != COUNT * PRODS)
            {
                return false;
            }
            return true;
        }
    }
    class HaltException : Exception
    {
    }

    interface BufferInterface
    {
        void Put(Object x);

        Object Get();

        void Halt();
    }

    class Buffer : BufferInterface
    {
        protected int SIZE;
        protected Object[] array;
        protected int putPtr = 0;
        protected int getPtr = 0;
        protected int usedSlots = 0;
        protected bool halted = false;

        public Buffer(int b)
        {
            SIZE = b;
            array = new Object[b];
        }

     
        public void Put(Object x)
        {
            lock (this)
            {
                while (usedSlots == SIZE)
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                array[putPtr] = x;
                putPtr = (putPtr + 1) % SIZE;

                if (usedSlots == 0)
                {
                    Monitor.PulseAll(this);
                }

                usedSlots++;
            }
        }

       
        public Object Get()
        {
            lock (this)
            {
                while ((usedSlots == 0) & !halted)
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException e)
                    {
                        Console.WriteLine(e.Message);
                        Environment.Exit(1);
                    }
                }

                Object x = array[getPtr];
                array[getPtr] = null;
                getPtr = (getPtr + 1) % SIZE;

                if (usedSlots == SIZE)
                {
                    Monitor.PulseAll(this);
                }

                usedSlots--;

                return x;
            }
        }

        public void Halt()
        {
            lock (this)
            {
                halted = true;
                Monitor.PulseAll(this);
            }
        }
    }

    class Attribute
    {
        public int attr;

        public Attribute()
        {
            attr = 0;
        }

        public Attribute(int attr)
        {
            this.attr = attr;
        }
    }

    class AttrData : Attribute
    {
        public int data;

        public AttrData(int attr, int data)
        {
            this.attr = attr;
            this.data = data;
        }
    }

    class Producer
    {
        private Buffer buffer;
        private Thread pThread;

        public Producer (Buffer b)
        {
            buffer = b;
            pThread = new Thread(new ThreadStart(Run));
            pThread.Start();
        }

        public void Run()
        {
            for (int i = 0; i < ChessTest.COUNT; i++)
            {
                AttrData ad = new AttrData(i, 1);
                buffer.Put(ad);
            }
            buffer.Halt();
        }

        public void JoinThread()
        {
            pThread.Join();
        }
    }

    class Consumer
    {
        private Buffer buffer;
        private Thread cThread;

        public Consumer(Buffer b)
        {
            buffer = b;
            cThread = new Thread(new ThreadStart(Run));
            cThread.Start();
        }

        public void Run()
        {
            int count = 0;
            AttrData[] received = new AttrData[10];

            while (count < received.Length)
            {
                received[count] = (AttrData) buffer.Get();
                if(received[count] == null)
                {
                    break;
                }
                else
                {
                    Inc(received[count].data);
                }
                count++;
            }
        }

        public void Inc(int x)
        {
            lock (this)
            {
                ChessTest.total += x;
            }
        }

        public void JoinThread()
        {
            cThread.Join();
        }
    }
}
