﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Account
{
    
    public class ChessTest
    {

     
        public static void Main(String[] args)
        {
        }
        
        public static Boolean Run()
        {
            try
            {
                ManageAccount.num = 4;
                ManageAccount[] bank = new ManageAccount[ManageAccount.num];
                Thread[] bankThreads = new Thread[ManageAccount.num];
                String[] accountName = { "A","B","C","D","E","F","G","H","I","J"};
                
                for (int j = 0; j < ManageAccount.num; j++)
                {
                    bank[j] = new ManageAccount(accountName[j], 100);
                    ManageAccount.accounts[j].Print();
                    bankThreads[j] = new Thread(new ThreadStart(bank[j].Run));
                }

                for (int k = 0; k < ManageAccount.num; k++)
                {
                    bankThreads[k].Start();
                }

                for (int k = 0; k < ManageAccount.num; k++)
                {
                    bankThreads[k].Join();
                }

                ManageAccount.PrintAll();

                bool bug = false;

                for (int k = 0; k < ManageAccount.num; k++)
                {
                    if (ManageAccount.accounts[k].amount < 300)
                    {
                        bug = true;
                    }
                    else if (ManageAccount.accounts[k].amount > 300)
                    {
                        bug = true;
                    }

                    if (bug)
                    {
                        throw new Exception("Bug found");
                    }
            
                }
            }
            catch (ThreadInterruptedException e)
            {
            }
            return true;
        }
    }
}
