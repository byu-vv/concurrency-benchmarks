﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account
{
    class ManageAccount
    {
        volatile Account account;
        public static Account[] accounts = new Account[10];
        public static int num = 2;
        static int accNum = 0;
        int i;

        public ManageAccount(string name, double amount)
        {
            account = new Account(name, amount);
            i = accNum;
            accounts[i] = account;
            accNum = (accNum + 1) % num;
        }

        public void Run()
        {
            account.Depsite(300);
            account.Transfer(accounts[(i + 1) % num], 10);
            account.Depsite(10);
            account.Transfer(accounts[(i + 2) % num], 10);
            account.Withdraw(20);
            account.Depsite(10);
            account.Transfer(accounts[(i + 1) % num], 10);
            account.Withdraw(100);
        }

        static public void PrintAll()
        {
            for (int j = 0; j < num; j++)
            {
                if (ManageAccount.accounts[j] != null)
                {
                    ManageAccount.accounts[j].Print();
                }
            }
        }
    }
}
