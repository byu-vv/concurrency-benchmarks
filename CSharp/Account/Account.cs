﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Account
{
    class Account
    {
        public double amount;
        volatile string name;

        public Account(string nm, double amnt)
        {
            amount = amnt;
            name = nm;
        }

        public void Depsite(double money)
        {
            lock (this)
            {
                amount += money;
            }
        }

        public void Withdraw(double money)
        {
            lock (this)
            {
                amount -= money;
            }
        }

        public void Transfer(Account ac, double mn)
        {
            lock (this)
            {
                amount -= mn;

                if (name.Equals("D"))
                {
                   // Console.WriteLine("Unprotected");
                    ac.amount += mn;
                }
                else
                {
                    lock (ac)
                    {
                        ac.amount += mn;
                    }
                }
            }
        }

        public void Print()
        {
            lock (this)
            {
            }
        }
    }
}
