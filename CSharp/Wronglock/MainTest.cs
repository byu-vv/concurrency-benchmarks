﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;


namespace WrongLock
{
   
    public class ChessTest
    {
        public static void Main(String[] args)
        {
            Console.WriteLine(Run());
        }
       
        public static Boolean Run()
        {
            int iNum1 = 10;
            int iNum2 = 1;

            TClass1[] class1 = new TClass1[iNum1];
            TClass2[] class2 = new TClass2[iNum2];

            Data data = new Data();
            Wronglock w1 = new Wronglock(data);

            for (int i = 0; i < iNum1; i++)
            {
                class1[i] = new TClass1(w1);
            }

            for (int i = 0; i < iNum2; i++)
            {
                class2[i] = new TClass2(w1);
            }

            for (int i = 0; i < iNum1; i++)
            {
                class1[i].joinTheThread();
            }

            for (int i = 0; i < iNum2; i++)
            {
                class2[i].joinTheThread();
            }
            return true;
        }
    }
}
