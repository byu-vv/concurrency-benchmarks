﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace WrongLock
{
    public class TClass2
    {
        Wronglock w1;
        Thread tClass2;

        public TClass2(Wronglock w1)
        {
            this.w1 = w1;
            tClass2 = new Thread(new ThreadStart(run));
            tClass2.Start();
        }

        public void run()
        {
            w1.B();
        }

        public void joinTheThread()
        {
            tClass2.Join();
        }
    }
}
