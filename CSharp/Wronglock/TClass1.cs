﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace WrongLock
{
    public class TClass1
    {
        Wronglock w1;
        Thread tClass1; 

        public TClass1(Wronglock w1)
        {
            this.w1 = w1;
            tClass1 = new Thread(new ThreadStart(run));
            tClass1.Start();
         }

        public void joinTheThread()
        {
            tClass1.Join();
        }

        public void run()
        {
            w1.A();
        }

    }
}
