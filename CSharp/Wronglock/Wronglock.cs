﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace WrongLock
{
    public class Wronglock
    {
        volatile Data data;
        public Wronglock(Data data)
        {
            this.data = data;
        }

        public void A()
        {
            lock (data)
            {
                int x = data.value;
                data.value++;
                if (data.value != x + 1)
                {
                    Console.WriteLine("Bug Found");
                    Environment.Exit(1);
                }
                
            }
        }

        public void B()
        {
            lock (this)
            {
                data.value++;
            }
        }
    }
}
