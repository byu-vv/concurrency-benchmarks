﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Airline
{
    

    public class Bug
    {
        
        Thread[] threadArr;
        volatile shared Shared;

        public Bug(int size, int cushion) 
        {
            int Num_of_tickets_issued = size;
            int Maximum_Capacity = Num_of_tickets_issued - cushion;
            threadArr = new Thread[Num_of_tickets_issued];
            Shared = new shared(Maximum_Capacity, Num_of_tickets_issued);
            Thread sale = new Thread(new ThreadStart(makeSale));
            sale.Start();
            
            for (int i = 0; i < Shared.Num_of_tickets_issued; i++)
            {   
             
                
                
                if (Shared.StopSales)
                {
                //     Console.Write("The value of Bug.StopSales" + Shared.StopSales);
                    Shared.sell--;
                    Shared.Num_Of_Seats_Sold--;   
                    break;
                }

                 Thread arr = new Thread(new ThreadStart(run));
                 arr.Start();
                 arr.Join();
            }

            
            if (Shared.sell > Shared.Maximum_Capacity)
            {
                Console.WriteLine("bug found" + Shared.sell);
                Environment.Exit(1);
            }
            sale.Join();
        }

        public void makeSale()
        {
            for (int i = 0; i < Shared.Num_of_tickets_issued; i++)
            {
       
                    if (Shared.sell == (Shared.Num_Of_Seats_Sold - 1))
                    {
                        Shared.sell++;
                        if (Shared.sell > (Shared.Maximum_Capacity))
                        {
                            Shared.StopSales = true;
                            return;
                        }
                    }
                
            }
        }
        public void run()
        {
         Shared.Num_Of_Seats_Sold++;
        }

    }

    class shared
    {
        public volatile int Num_Of_Seats_Sold = 0;
        public volatile int Maximum_Capacity, Num_of_tickets_issued;
        public volatile int sell = 0;
        public volatile Boolean StopSales = false;
 

        public shared(int Maximum_Capacity, int Num_of_tickets_issued)
        {
            this.Maximum_Capacity = Maximum_Capacity;
            this.Num_of_tickets_issued = Num_of_tickets_issued;
        }
    }

}


