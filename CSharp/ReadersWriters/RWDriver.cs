using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ReadersWriters
{
    public class ChessTest
    {
        static RWPrinter rwp;
        public static IntWrapper iw;
        public static int bound;
        static int reading;

		public static void Main(string[] args)
		{
			ChessTest.Run();
		}
		
        public static bool Run()
        {
            rwp = new RWPrinter();
            iw = new IntWrapper();

            int readers = 2;
            int writers = 2;
            bound = 100;
            reading = 0;

            Reader[] readerArray = new Reader[readers];
            for (int i = 0; i < readers; i++)
            {
                readerArray[i] = new Reader(rwp, i);
                readerArray[i].Start();
            }

            Writer[] writerArray = new Writer[writers];
            for (int i = 0; i < writers; i++)
            {
                writerArray[i] = new Writer(rwp, i);
                writerArray[i].Start();
            }

            for (int i = 0; i < readers; i++)
                readerArray[i].Join();
            for (int i = 0; i < writers; i++)
                writerArray[i].Join();
			return true;
        }
    }

    public class IntWrapper
    {
        public int x = 0;
    }

    sealed class Reader
    {
        protected RWPrinter rwp;
        protected int id;
        protected Thread t;

        public Reader(RWPrinter r, int id)
        {
            rwp = r;
            this.id = id;
            t = new Thread(new ThreadStart(this.Run));
        }
        public void Run()
        {
            for (int i = 0; i < ChessTest.bound; i++)
                rwp.Read();
        }
        public void Start()
        {
            t.Start();
        }
        public void Join()
        {
            t.Join();
        }
    }

    sealed class Writer
    {
        protected RWPrinter rwp;
        protected int id;
        protected Thread t;

        public Writer(RWPrinter r, int id)
        {
            rwp = r;
            this.id = id;
            t = new Thread(new ThreadStart(this.Run));
        }
        public void Run()
        {
            for (int i = 0; i < ChessTest.bound; i++)
                rwp.Write();
        }
        public void Start()
        {
            t.Start();
        }
        public void Join()
        {
            t.Join();
        }
    }

    class RWPrinter : RWVSN
    {
        protected int reading = 0;
        protected override void DoRead()
        {
            lock (this)
            {
                reading++;
            }
            if (ChessTest.iw.x < 10)
            {
                ChessTest.iw.x++;
            }
            lock (this)
            {
                reading--;
            }
        }
        protected override void DoWrite()
        {
            if (reading > 0)
            {
                throw new Exception("bug found");
            }
            if (ChessTest.iw.x > 0)
            {
                ChessTest.iw.x--;
            }
        }
    }

    abstract class RWVSN
    {
        protected int activeReaders = 0;
        protected int activeWriters = 0;

        protected int waitingReaders = 0;
        protected int waitingWriters = 0;

        protected abstract void DoRead();
        protected abstract void DoWrite();

        public void Read()
        {
            BeforeRead();
            try
            {
                DoRead();
            }
            finally
            {
                AfterRead();
            }
        }

        public void Write()
        {
            BeforeWrite();
            try
            {
                DoWrite();
            }
            finally
            {
                AfterWrite();
            }
        }

        protected bool AllowReader()
        {
            return (waitingWriters == 0 && activeWriters == 0);
        }

        protected bool AllowWriter()
        {
            return (activeReaders == 0 && activeWriters == 0);
        }

        protected void BeforeRead()
        {
            lock (this)
            {
                waitingReaders++;
                while (!AllowReader())
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException ex)
                    {
                        waitingReaders--;
                    }
                }
                waitingReaders--;
            }
            int temp = activeReaders;
            temp++;
            activeReaders = temp;
        }

        protected void AfterRead()
        {
            lock (this)
            {
                activeReaders--;
                Monitor.PulseAll(this);
            }
        }

        protected void BeforeWrite()
        {
            lock (this)
            {
                waitingWriters++;
                while (!AllowWriter())
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException ex)
                    {
                        waitingWriters--;
                    }
                }
                waitingWriters--;
                activeWriters++;
            }
        }

        protected void AfterWrite()
        {
            lock (this)
            {
                activeWriters--;
                Monitor.PulseAll(this);
            }
        }
    }
}
