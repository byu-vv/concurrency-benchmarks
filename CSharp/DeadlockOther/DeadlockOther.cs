﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DeadlockOther
{

    public class ChessTest
    {
        public static Lock lock1;
        public static Lock lock2;
        public static int state;

		public static void Main(string[] args)
		{
		}
		
        public static bool Run()
        {
            lock1 = new Lock();
            lock2 = new Lock();
            Process1 p1 = new Process1();
            Process2 p2 = new Process2();
            p1.Start();
            p2.Start();
            p1.Join();
            p2.Join();
			return true;
        }
    }

    class Process1
    {
        Thread t;
        public Process1()
        {
            t = new Thread(new ThreadStart(this.Run));
        }

        public void Start()
        {
            t.Start();
        }

        public void Join()
        {
            t.Join();
        }

        public void Run()
        {
            ChessTest.state++;
            lock (ChessTest.lock1)
            {
                lock (ChessTest.lock2)
                {
                    ChessTest.state++;
                }
            }
        }
    }

    class Process2
    {
        Thread t;
        public Process2()
        {
            t = new Thread(new ThreadStart(this.Run));
        }

        public void Start()
        {
            t.Start();
        }

        public void Join()
        {
            t.Join();
        }

        public void Run()
        {
            ChessTest.state++;
            lock (ChessTest.lock2)
            {
                lock (ChessTest.lock1)
                {
                    ChessTest.state++;
                }
            }
        }
    }

    public class Lock
    { }
}
