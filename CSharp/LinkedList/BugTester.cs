﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace LinkedList
{

    public class ChessTest
    {
		public static void Main(string[] args)
        {
			Console.WriteLine("HELLO");
			Console.WriteLine(Run());
        }
	
        public static bool Run()
        {
            int builders = 4;
            int maxsize = 100;

            int step = maxsize / builders;
            Thread[] threads = new Thread[builders];

            try
            {
                MyLinkedList mlst = new MyLinkedList(maxsize);
                MyListBuilder mlist = null;

                for (int i = 0; i < builders; i++)
                {
                    mlist = new MyListBuilder(mlst, i * step, (i + 1) * step, true);
                    threads[i] = new Thread(new ThreadStart(mlist.Run));
                }

                for (int i = 0; i < builders; i++)
                    threads[i].Start();

                for (int i = 0; i < builders; i++)
                    threads[i].Join();

                mlist.Print();

                mlist.Empty();
            }
            catch (ThreadInterruptedException e)
            {
				Console.WriteLine("Interrupted Exception found.");
                throw new Exception("Interrupted Exception found.");
            }
			return true;
        }
    }
}
