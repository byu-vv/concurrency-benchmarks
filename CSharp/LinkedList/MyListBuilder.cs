﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LinkedList
{
    class MyListBuilder
    {
        public bool _debug = true;
        public Object _list = null;
        public int _bound1 = 0;
        public int _bound2 = 0;

        public MyListBuilder(Object lst, int bnd1, int bnd2, bool dbg)
        {
            this._debug = dbg;
            this._list = (MyLinkedList)lst;
            this._bound1 = bnd1;
            this._bound2 = bnd2;
        }

        public void Run()
        {
            for (int i = this._bound1; i < this._bound2; i++)
            {
                Int32 intObject = i;
                ((MyLinkedList)_list).AddLast(intObject);
            }
        }

        public void Print()
        {
            int size;
            int x;

            size = ((MyLinkedList)_list).Size();

            try
            {
                ((MyLinkedList)this._list).PrintList((MyLinkedList)_list);
            }
            catch (IOException e)
            {
            }
        }

        public void Empty()
        {
            ((MyLinkedList)_list).Clear();
        }
    }
}
