﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkedList
{
    class MyLinkedList
    {
        private volatile MyListNode _header;
        private int _maxsize;

        public MyLinkedList(int n)
        {
            this._header = new MyListNode(null);
            this._maxsize = n;
        }

        public bool IsEmpty()
        {
            return (this._header._next == null);
        }

        public void Clear()
        {
            this._header._next = null;
        }

        public MyLinkedListItr First()
        {
            return new MyLinkedListItr(this._header._next);
        }

        public void Insert(Object x, MyLinkedListItr p)
        {
            if (p != null && p._current != null)
            {
                MyListNode tmp;

                lock (this)
                {
                    tmp = new MyListNode(x, p._current._next);
                }

                p._current._next = tmp;
            }
        }

        public void AddLast(Object x)
        {
            MyListNode itr = this._header;
            while (itr._next != null)
                itr = itr._next;
            Insert(x, new MyLinkedListItr(itr));
        }

        public int Size()
        {
            MyListNode itr = this._header;
            int i = 0;

            while (itr._next != null)
            {
                i++;
                itr = itr._next;
            }

            return i;
        }

        public MyLinkedListItr Find(Object x)
        {
            MyListNode itr = this._header._next;

            while (itr != null && !itr._element.Equals(x))
                itr = itr._next;

            return new MyLinkedListItr(itr);
        }

        public MyLinkedListItr FindPrevious(Object x)
        {
            MyListNode itr = this._header;

            while (itr._next != null && !itr._next._element.Equals(x))
                itr = itr._next;

            return new MyLinkedListItr(itr);
        }

        public void Remove(Object x)
        {
            MyLinkedListItr p = FindPrevious(x);

            if (p._current._next != null)
                p._current._next = p._current._next._next;
        }

        public void PrintList(MyLinkedList theList)
        {
            int x;
            if (theList.IsEmpty())
                x = 1;
            else
            {
                MyLinkedListItr itr = theList.First();
                for (; !itr.IsPastEnd(); itr.Advance())
                    x = 1;
            }

            if (this.Size() != this._maxsize)
			{
				Console.WriteLine("bug found");
				Environment.Exit(1);
                //throw new Exception("bug found");
			}

        }
    }
}
