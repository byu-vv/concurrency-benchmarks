﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkedList
{
    class MyLinkedListItr
    {
        public volatile MyListNode _current;

        public MyLinkedListItr(MyListNode theNode)
        {
            this._current = theNode;
        }

        public bool IsPastEnd()
        {
            return (this._current == null);
        }

        public Object Retrieve()
        {
            return IsPastEnd() ? null : this._current._element;
        }

        public void Advance()
        {
            if (!IsPastEnd())
                this._current = this._current._next;
        }
    }
}
