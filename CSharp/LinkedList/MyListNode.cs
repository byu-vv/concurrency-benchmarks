﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkedList
{
    class MyListNode
    {
        public volatile Object _element;
        public volatile MyListNode _next;

        public MyListNode(Object theElement):this(theElement, null)
        {
        }

        public MyListNode(Object theElement, MyListNode n)
        {
            lock (this)
            {
                this._element = theElement;
                this._next = n;
            }
        }
    }
}
