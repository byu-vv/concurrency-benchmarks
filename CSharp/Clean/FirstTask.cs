﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace clean
{
    class FirstTask
    {
        Thread firstThread;
        int iterations;
        Event event1, event2;

        public FirstTask(Event e1, Event e2, int iterations)
        {
            this.event1 = e1;
            this.event2 = e2;
            this.iterations = iterations;
            firstThread = new Thread(new ThreadStart(Run));
            firstThread.Start();
        }

        public void Run()
        {
            int count = 0;
            count = event1.count;
            for (int i = 0; i < iterations; i++)
            {
                event1.WaitForEvent(count);
                count = event1.count;
                event2.SignalEvent();
            }
        }

        public void JoinThread()
        {
            firstThread.Join();
        }
    }
}
