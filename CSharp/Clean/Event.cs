﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.CompilerServices;

namespace clean
{
    class Event
    {
        public int count = 0;

     
        public void WaitForEvent(int remoteCount)
        {
            lock(this){
            if (remoteCount == count)
            {
                try
                {
                    Monitor.Wait(this);
                }
                catch (ThreadInterruptedException e)
                { }
            }
                
            }
        }

        public void SignalEvent()
        {
            count = (count + 1) % 100;
            Monitor.PulseAll(this);
        }
    }
}
