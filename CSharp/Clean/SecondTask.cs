﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace clean
{
    class SecondTask
    {
        Thread secondThread;
        int iterations;
        Event event1, event2;

        public SecondTask(Event e1, Event e2, int iterations)
        {
            this.event1 = e1;
            this.event2 = e2;
            this.iterations = iterations;
            secondThread = new Thread(new ThreadStart(Run));
            secondThread.Start();
        }

        public void Run()
        {
            int count = 0;
            count = event2.count;
            for (int i = 0; i < iterations; i++)
            {
                event1.SignalEvent();
                event2.WaitForEvent(count);
                count = event2.count;
            }
        }

        public void JoinThread()
        {
            secondThread.Join();
        }
    }
}
