﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;


namespace clean
{
    
    public class ChessTest
    {
        static int iFirstTask = 1;
        static int iSecondTask = 1;
        static int iterations = 12;

        public static void Main(String[] args)
        {
            Console.WriteLine(Run());
        }

      
        public static Boolean Run()
        {
            Event newEvent1 = new Event();
            Event newEvent2 = new Event();

            FirstTask[] firstTasks = new FirstTask[iFirstTask];
            SecondTask[] secondTasks = new SecondTask[iSecondTask];

            for (int i = 0; i < iFirstTask; i++)
            {
                // create and run threads here
                firstTasks[i] = new FirstTask(newEvent1, newEvent2, iterations);
            }
            for (int i = 0; i < iSecondTask; i++)
            {
                // create and run threads here
                secondTasks[i] = new SecondTask(newEvent1, newEvent2, iterations);
            }

            for (int i = 0; i < iFirstTask; i++)
            {
                firstTasks[i].JoinThread();
            }
            for (int i = 0; i < iSecondTask; i++)
            {
                secondTasks[i].JoinThread();
            }
            return true;
        }
    }
}
