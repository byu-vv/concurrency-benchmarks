﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Piper
{
    public class Producer
    {
        private Piper _piper;
        private String _name;
        static int NUM_OF_SEATS;
        Thread producer;

        public Producer(Piper p, String n, int numOfSeats)
        {
            _piper = p;
            _name = n;
            NUM_OF_SEATS = numOfSeats;
            producer = new Thread(new ThreadStart(run));
            producer.Start();
        }

        public void run()
        {
            try
            {
                for (int i = 0; i < NUM_OF_SEATS; i++)
                {
                    _piper.fillPlane(_name);
                }
            }
            catch (Exception) { }
        }

        public void joinTheThread()
        {
            producer.Join();
        }
    }
}
