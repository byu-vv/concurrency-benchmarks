﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Piper
{
    public class Consumer
    {
        private Piper _piper;
        static int NUM_OF_SEATS;
        Thread consumer;

        public Consumer(Piper p, int numOfSeats)
        {
            _piper = p;
            NUM_OF_SEATS = numOfSeats;
            consumer = new Thread(new ThreadStart(run));
            consumer.Start();
        }

        public void run()
        {
            try
            {
                for (int i = 0; i < NUM_OF_SEATS; i++)
                {
                    _piper.emptyPlane();
                }
            }
            catch (Exception) { }
        }

        public void joinTheThread()
        {
            consumer.Join();
        }
    }
}
