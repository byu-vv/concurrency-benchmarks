﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace Piper
{
    public class ChessTest
    {

        public static void Main(String[] args)
        {
            Console.WriteLine(Run());
        }

        
        public static Boolean Run()
        {
            int NUM_OF_SEATS = 2;
            int _numberOfThreads = 8;   
            int numSeats = 7;

            Piper p = new Piper("file", _numberOfThreads * NUM_OF_SEATS, numSeats);
            Consumer[] cons = new Consumer[_numberOfThreads + 1];
            Producer[] prod = new Producer[_numberOfThreads + 1];

            for (int i = 0; i <= _numberOfThreads; i++)
            {
                cons[i] = new Consumer(p, NUM_OF_SEATS);
                prod[i] = new Producer(p, "passenger", NUM_OF_SEATS);
            }

            for (int i = 0; i <= _numberOfThreads; i++)
            {
                cons[i].joinTheThread();
                prod[i].joinTheThread();
            }
            return true;
        }
    }
}
