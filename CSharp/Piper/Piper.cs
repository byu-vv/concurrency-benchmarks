﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Piper
{
    public class Piper
    {
        static int NUM_OF_SEATS = 2;
        private int _first, _last;
        private String[] _passengers;
        private String _fileName;
        static int _fillCount;
        static int _emptyCount;

        public Piper(String fileName, int fillCount,
            int numSeats)
        {
            _first = 0;
            _last = 0;
            NUM_OF_SEATS = numSeats;
            _passengers = new String[NUM_OF_SEATS];
            _fileName = fileName;
            _fillCount = _emptyCount = fillCount;
            setFile();
        }

        public void setFile()
        {
        }

        public void fillPlane(String name)
        {
            lock (this)
            {
                try
                {
                    if ((_last + 1) % NUM_OF_SEATS == _first)
                        Monitor.Wait(this, Timeout.Infinite);
                }
                catch (ThreadInterruptedException) { }
            }

            lock (this)  // the wait will relinquish the locks
            {
                if (--_fillCount == 0) closeFile();
            }

            _last = (_last + 1) % NUM_OF_SEATS;
            Monitor.PulseAll(this);
        }

        public String emptyPlane()
        {
            lock (this)
            {
                try
                {
                    while (_first == _last)
                        Monitor.Wait(this, Timeout.Infinite);
                }
                catch (ThreadInterruptedException) { }


                String name = _passengers[_first];
                lock (this)
                {
                    Piper._emptyCount--;
                }

                _first = (_first + 1) % NUM_OF_SEATS;
                Monitor.PulseAll(this);

                return name;
            }
        }

        public void closeFile()
        {
            //DO NOTHING
        }
    }
}
