﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    class Task
    {
        private Shared shared;

        public Task(Shared shared)
        {
            this.shared = shared;
            //System.out.println("starting task");
            //this.Start();
        }

        public void Run()
        {
            while (true)
            {
                //System.out.println("task running");
                shared.Write(0);
            }
        }
    }
}
