﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    class Observer
    {
        private Plan plan;

        public Observer(Plan plan)
        {
            this.plan = plan;
            //System.out.println("starting observer");
            //this.Start();
        }

        public void run()
        {
            int currentToken;
            while (true)
            {
                //System.out.println("observer loop");
                currentToken = plan.GetCurrentToken();
            }
        }
    }
}
