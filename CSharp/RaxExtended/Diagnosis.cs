﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    class Diagnosis
    {
        private Plan plan;

        public Diagnosis(Plan plan)
        {
            this.plan = plan;
            //System.out.println("starting diagnosis");
            //this.Start();
        }

        public void Run()
        {
            while (true)
            {
                //System.out.println("diagnosis loop");
                Events.exec.WaitForEvent();
                plan.Repair();
            }
        }
    }
}
