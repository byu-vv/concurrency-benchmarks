﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RaxExtended
{
    class Environment
    {
        public static void Create(int gc)
        {
            for (int i = 0; i < gc; i++)
            {
                CreateGroup();
            }
        }

        public static void CreateGroup()
        {
            Shared shared = new Shared();
            //new Task(shared);
            Task task1 = new Task(shared);
            Thread t1 = new Thread(new ThreadStart(task1.Run));
            t1.Start();
            //new Task(shared);
            Task task2 = new Task(shared);
            Thread t2 = new Thread(new ThreadStart(task2.Run));
            t2.Start();
        }
    }
}
