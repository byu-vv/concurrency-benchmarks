﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    class Plan
    {
        private int[] timeline = new int[5];
        private int currentToken = 0;

        public void Generate()
        {
            lock (this)
            {
                timeline[0] = 10;
                timeline[1] = 11;
                timeline[2] = 12;
                timeline[3] = 13;
                timeline[4] = 14;
                currentToken = 0;
            }
        }

        public int GetCurrentToken()
        {
            lock (this)
            {
                return currentToken;
            }
        }

        public bool Done()
        {
            lock (this)
            {
                return currentToken == 5;
            }
        } 

        public void ExecuteToken()
        {
            lock (this)
            {
                currentToken++;
            }
        }

        public void Repair()
        {
            lock (this)
            {
                currentToken = 0;
            }
        }
    }
}
