﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RaxExtended
{
    public class Event
    {
        public int count = 0;
        private int wrapCount = 3; 

        public Event(int wc) { wrapCount = wc; }

        public void WaitForEvent()
        {
            lock(this)
            {
                try{Monitor.Wait(this);}catch(ThreadInterruptedException e){};
            }
        }

        public void SignalEvent()
        {
            lock (this)
            {
                count = (count + 1) % wrapCount;
                //System.out.println("count is "+count);
                Monitor.PulseAll(this);
            }
        }
    }
}
