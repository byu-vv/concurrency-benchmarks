﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    public class Events
    {
        public static Event plan;
        public static Event exec;

        public static void Initialize(int wc)
        {
            plan = new Event(wc);
            exec = new Event(wc);
        }
    }
}
