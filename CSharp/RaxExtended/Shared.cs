﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    class Shared
    {
        private int x = 0;

        public void Write(int i)
        {
            lock (this)
            {
                x = i;
            }
        }
    }
}
