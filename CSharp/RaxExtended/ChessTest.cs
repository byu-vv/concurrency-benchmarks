using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace RaxExtended
{

    public class ChessTest
    {
        public static void Main(string[] args)
        {
			Console.WriteLine(Run());
        }
		
		public static bool Run()
		{
			int gc = 10;
            int wc = 3;

            //Environment.create();

            Events.Initialize(wc);
            Plan plan = new Plan();
            //new Planner(plan);
            Planner pl = new Planner(plan);
            Thread t1 = new Thread(new ThreadStart(pl.Run));
            t1.Start();
            //new Executive(plan);
            Executive ex = new Executive(plan);
            Thread t2 = new Thread(new ThreadStart(ex.Run));
            t2.Start();
            //new Diagnosis(plan);
            Diagnosis diag = new Diagnosis(plan);
            Thread t3 = new Thread(new ThreadStart(diag.Run));
            t3.Start();
            Environment.Create(gc);
			
			t1.Join();
            t2.Join();
            t3.Join();
			return true;
		}
    }
}
