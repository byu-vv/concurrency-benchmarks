﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    class Planner
    {
        private Plan plan;
        private int count = 0;

        public Planner(Plan plan)
        {
            this.plan = plan;
            //System.out.println("starting planner");
            //this.Start();
        }

        public void Run()
        {
            count = Events.plan.count;
            while (true)
            {
                //System.out.println("planner loop with count ="+count+", Events.plan.count ="+Events.plan.count);
                if (count == Events.plan.count)
                {
                    //System.out.println("testing with count ="+count+", Events.plan.count ="+Events.plan.count);
                    if (count != Events.plan.count)
                    {
                        throw new Exception("bug");
                    }

                    Events.plan.WaitForEvent();
                }
                count = Events.plan.count;
                plan.Generate();
                Events.exec.SignalEvent();
            }
        }
    }
}
