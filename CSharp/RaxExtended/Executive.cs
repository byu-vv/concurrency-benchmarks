﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxExtended
{
    class Executive
    {
        private Plan plan;
        private int count = 0;

        public Executive(Plan plan)
        {
            this.plan = plan;
            //System.out.println("starting executive");
            //this.Start();
        }

        public void Run()
        {
            count = Events.exec.count;
            while (true)
            {
                //System.out.println("executive loop");
                Events.plan.SignalEvent();
                if (count == Events.exec.count)
                    Events.exec.WaitForEvent();
                count = Events.exec.count;
                while (!plan.Done())
                {
                    plan.ExecuteToken();
                }
            }
        }
    }
}
