﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ReplicatedWorkers
{
    class AQWork : WorkItemComputation
    {
        public float start;
	    public float end;
	    public float epsilon;
    	
	    public AQWork()
	    { }
    	
	    public AQWork(float s, float e, float eps)
	    {
		    start = s;
		    end = e;
		    epsilon = eps;
	    }
    	
	    public String toString()
	    {
		    return "AQWork.interval[" + start + ", " + end + ", " + epsilon + "]";
	    }
    	
	    public float f(float x)
	    {
		    return x * x - x;
	    }
    	
	    public bool DoWork(ArrayList newWork, ArrayList theResults)
	    {
		    float x1, x2, x3;
    		
		    x1 = (float)((end + start)/2.0);
		    x2 = (float)((start + x1)/2.0);
		    x3 = (float)((end + x1)/2.0);
    		
		    if (Math.Abs(f(x2)/2.0 + f(x3)/2.0 - f(x1))/10.0 >= epsilon)
		    {
			    newWork.Add(new AQWork(start, x1, epsilon));
			    newWork.Add(new AQWork(x1, end, epsilon));
		    }
		    else
		    {
			    theResults.Add(new AQResults((x1-start)*(f(x2)+f(x3))));
		    }
		    return false;
	    }
    }
}
