﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ReplicatedWorkers
{
    interface WorkItemComputation
    {
        bool DoWork(ArrayList newWork, ArrayList results);
    }
}
