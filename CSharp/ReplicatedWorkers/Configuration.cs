﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReplicatedWorkers
{
    public class Configuration
    {
        public const int EXCLUSIVE = 1, CONCURRENT = 2, NONE = 3;
        private int theResultSemantics;

        public const int SYNCHRONOUS = 1, ASYNCHRONOUS = 2;
        private int theExecuteSemantics;

        public const int ALLVALUES = 1, SOMEVALUES = 2;
        private int theSubProblemSemantics;

        public Configuration() 
        {
            theResultSemantics = NONE; 
            theExecuteSemantics = SYNCHRONOUS; 
            theSubProblemSemantics = ALLVALUES; 
        }

        public Configuration(int call, int exec, int subprob) 
        {
            theResultSemantics = call; 
            theExecuteSemantics = exec; 
            theSubProblemSemantics = subprob; 
        }

        public bool IsResultExclusive() {return theResultSemantics == EXCLUSIVE;}
        public bool IsResultConcurrent() {return theResultSemantics == CONCURRENT;}
        public bool IsResultNone() {return theResultSemantics == NONE;}

        public bool IsSynchronous() {return theExecuteSemantics == SYNCHRONOUS;}
        public bool IsAsynchronous() {return theExecuteSemantics == ASYNCHRONOUS;}

        public bool IsAll() {return theSubProblemSemantics == ALLVALUES;}
        public bool IsSome() {return theSubProblemSemantics == SOMEVALUES;}
    }
}
