﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ReplicatedWorkers
{
    public sealed class StandardBarrier
    {
        private long participants;
        private long numBlocked, numReleased;
        private bool allBlocked, allReleased;

        public StandardBarrier(long initial)
        {
            participants = initial;
            numReleased = numBlocked = 0;
        }

        private void BlockAll()
        {
            lock (this)
            {
                ++numBlocked;
                allBlocked = false;
                while (numBlocked < participants && !allBlocked)
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException ex)
                    {
                    }
                }
                allBlocked = true;
                numBlocked = 0;
                Monitor.PulseAll(this);
            }
        }

        private void ReleaseAll()
        {
            lock (this)
            {
                allReleased = false;
                while (numReleased < participants && !allReleased)
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException ex)
                    {
                    }
                }
                allReleased = true;
                numReleased = 0;
                Monitor.PulseAll(this);
            }
        }

        public void Await()
        {
            lock (this)
            {
                BlockAll();
                ReleaseAll();
            }
        }
    }
}
