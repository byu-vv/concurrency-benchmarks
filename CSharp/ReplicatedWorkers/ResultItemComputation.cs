﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReplicatedWorkers
{
    interface ResultItemComputation
    {
        bool DoResults();
    }
}
