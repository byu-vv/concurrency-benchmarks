﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ReplicatedWorkers
{
    public sealed class SynchronizedCollection
    {
        private Collection theCollection;

        public SynchronizedCollection(Collection c)
        {
            theCollection = c; 
        } 

        public int Size()
        {
            lock(this)
            {
                return theCollection.Size();
            }
        }

        public ArrayList Take()
        {
            lock(this)
            {
                ArrayList returnVal = new ArrayList(theCollection.Size());
                for (int i=0; i<theCollection.Size(); i++) {
                    returnVal.Add(theCollection.Take());
                }
                return returnVal;
            }
        }

        public void Add(ArrayList al)
        {
            for (int i = 0; i < al.Count; i++)
                theCollection.Add(al[i]);

        }
    }
}
