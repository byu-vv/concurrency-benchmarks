﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace ReplicatedWorkers
{

    public class ChessTest
    {
        /**
         * The default number of workers to use.
         */
        public const int DEFAULT_NUM_WORKERS = 8;  // CHANGE from 3

        /**
         * The default number of work items.
         */
        public const int DEFAULT_NUM_ITEMS = 2;
        
        /**
         * The default minimum for the work item.
         */
        public const float DEFAULT_MIN = (float) 0.0;
        
        /**
         * The default maxmimum for the work item.
         */
        public const float DEFAULT_MAX = (float)10.0;
        
        /**
         * The default epsilon for the work item.
         */
        public const float DEFAULT_EPSILON = (float)0.001;   // CHANGE from 0.25

		public static void Main(string[] args)
		{
			Run();
		}

        public static bool Run()
        {

            /**
             * Start up an instance of the replicated workers example using
             * the command line arguments or the defaults.
             */
            ReplicatedWorkers theInstance;
            Configuration theConfig;
            FIFOCollection workPool;
            FIFOCollection resultPool;
            ArrayList inputs;

            theConfig = new Configuration(Configuration.EXCLUSIVE, Configuration.SYNCHRONOUS,
                    Configuration.SOMEVALUES);
            workPool = new FIFOCollection();
            resultPool = new FIFOCollection();

            int numWorkers = DEFAULT_NUM_WORKERS;
            int numItems = DEFAULT_NUM_ITEMS;
            float min = DEFAULT_MIN;
            float max = DEFAULT_MAX;
            float epsilon = DEFAULT_EPSILON;
            /*if ((args != null) && (args.length == 5)) {
                numWorkers = Integer.parseInt(args[0]);
                numItems = Integer.parseInt(args[1]);
                min = Float.parseFloat(args[2]);
                max = Float.parseFloat(args[3]);
                epsilon = Float.parseFloat(args[4]);
            }*/

            theInstance = new ReplicatedWorkers(theConfig, workPool, resultPool, numWorkers, numItems);
            inputs = new ArrayList(1);
            inputs.Add(new AQWork(min, max, epsilon));
            theInstance.PutWork(inputs);
            AQResults.totalSum = 0;
            theInstance.Execute();
            //System.out.println("result is "+AQResults.totalSum);
            //For EPSILON = .25
            //if ((AQResults.totalSum < 278.1) || (AQResults.totalSum > 278.15)) {
            //For EPSILON = .05
            if ((AQResults.totalSum < 282.0) || (AQResults.totalSum > 282.1)) {
            //	   throw new RuntimeException("bug");
            }
            theInstance.Destroy();
			return true;
        }
    }
}
