﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ReplicatedWorkers
{
    public sealed class StandardCountingSemaphore
    {
        protected long permits_;

        public StandardCountingSemaphore(long initial)
        {
            permits_ = initial;
        }

        public StandardCountingSemaphore()
        {
            permits_ = 0;
        }

        public void Await()
        {
            lock (this)
            {
                if (--permits_ < 0)
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException ex)
                    {
                    }
                }
            }
        }

        public void Signal()
        {
            lock (this)
            {
                if (permits_++ < 0)
                    Monitor.Pulse(this);
            }
        }
    }
}
