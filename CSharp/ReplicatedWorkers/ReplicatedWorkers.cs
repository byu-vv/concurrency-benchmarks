﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;

namespace ReplicatedWorkers
{
    public sealed class ReplicatedWorkers
    {
        private ArrayList workers;
        public bool stop;
        public bool done;
        public StandardCountingSemaphore resultLock;
        public StandardBarrier mainWorkerBarrier;
        public Collection workPool;
        public SynchronizedCollection resultPool;
        public Coordinator theCoord;
        public Configuration theConfig;
        public int numWorkers;
        public int numItems;

        public ReplicatedWorkers(Configuration newConfig,
                           Collection workCollection,
                           Collection resultCollection,
                           int newNumWorkers, int newNumItems)
        {
            done = true;
            stop = false;
            numWorkers = newNumWorkers;
            numItems = newNumItems;
            theConfig = newConfig;

            workPool = workCollection;
            resultPool = new SynchronizedCollection(resultCollection);
            resultLock = new StandardCountingSemaphore(1);
            mainWorkerBarrier = new StandardBarrier(numWorkers + 1);
            theCoord = new Coordinator(this);


            workers = new ArrayList(numWorkers);
            for (int i = 0; i < numWorkers; i++)
            {
                workers.Add(new Worker(this));
            }
        }

        public void Destroy()
        {
            lock(this)
            {
                stop = true; 
                mainWorkerBarrier.Await();
            }
        }

        public void PutWork(ArrayList v)
        {
            lock(this)
            {
                theCoord.Add(v); 
            }
        } // end public void putWork(Vector v)

        public ArrayList GetResults() 
        {
            lock (this)
            {
                return resultPool.Take();
            }
        } // end public Vector getResults()

        public void Execute()
        {
            lock (this)
            {
                done = false;
                mainWorkerBarrier.Await();

                // Add conditional for asynch execution

                // Detect termination
                //
                // Workers can signal termination based on local computation
                // results.  This will initiate a shutdown (release of all blocked
                // workers).
                //
                // Draining the work pool can also terminate the computation.
                // In which case we need to initiate a shutdown here.
                theCoord.MainAwaitTerminate();
                done = true;
                theCoord.NotifyDoneChange();

                // In any case, we need to make sure that all workers have
                // sucessfully terminated their computations and are awaiting
                // execution.
                mainWorkerBarrier.Await();
            }
        } // end public void execute()
    }

    public sealed class Coordinator
    {
        private int blockedTakers;
        private ReplicatedWorkers memberOf;

        public Coordinator(ReplicatedWorkers instance)
        {
            memberOf = instance;
            blockedTakers = 0;
        }

        public void NotifyTakerChange()
        {
            if ( blockedTakers == memberOf.numWorkers ) 
                Monitor.PulseAll(this);
        }

        public void NotifyDoneChange()
        {
            if ( memberOf.done ) 
                Monitor.PulseAll(this);
        }

        public void NotifyPoolChange()
        {
             if ( (memberOf.workPool.Size() >= memberOf.numItems) ||
                  (memberOf.theConfig.IsSome() && memberOf.workPool.Size() >= 0) )
                Monitor.PulseAll(this);
        }

        public void MainAwaitTerminate()
        {
            while ( blockedTakers != memberOf.numWorkers && 
                !memberOf.done ) 
            {
                try { Monitor.Wait(this); } catch (ThreadInterruptedException ex) {}
            }
        }

  // CHANGE: push synch inward
        public ArrayList Take(int num)
        {
            ++blockedTakers;

            lock(this) {
                if ( (memberOf.workPool.Size() < num) && 
                  // EASY BUG: change to "||" to "&&"
                  (!memberOf.theConfig.IsSome() || memberOf.workPool.Size() == 0) &&
                  !memberOf.done) 
                    memberOf.theCoord.NotifyTakerChange();

                // Only accept a get request if:
                //    workPool.size() >= num
                // or
                //    theConfig.isSome() & workPool.size() > 0
                // or
                //    done
                //
                // EASY BUG: change from "< num" to "== num"
                // EASY BUG: change from "while" to "if"
                while ( (memberOf.workPool.Size() < num) && 
                     (!memberOf.theConfig.IsSome() || memberOf.workPool.Size() == 0) &&
                     !memberOf.done) {
                    try { Monitor.Wait(this); } catch ( ThreadInterruptedException ex) {}
                }

                // Create a vector of up to num elements
                if (memberOf.done) {
                    --blockedTakers;
                    memberOf.theCoord.NotifyTakerChange();

                    return new ArrayList();

                } else {
                    ArrayList returnVal = new ArrayList(num);
                    if ( num > memberOf.workPool.Size() ) 
                        num = memberOf.workPool.Size();

                    for (int i=0; i<num; i++) {
                        returnVal.Add(memberOf.workPool.Take());
                    }

                    --blockedTakers;
                    memberOf.theCoord.NotifyTakerChange();

                    return returnVal;
                }
            }
        }

        public void Add(ArrayList al)
        {
            lock (this)
            {
                for (int i = 0; i < al.Count; i++)
                    memberOf.workPool.Add(al[i]);
                NotifyPoolChange();
            }
        }
    }
    sealed class Worker
    {
        private ReplicatedWorkers memberOf;
        private Thread thisThread;

        public Worker(ReplicatedWorkers rwInstance)
        {
            memberOf = rwInstance;
            thisThread = new Thread(new ThreadStart(this.Run));
            thisThread.Start();
        } // end public Worker()

        public void Join()
        {
            thisThread.Join();
        }

        public void Run()
        {
            ArrayList theWork, newWork, theResults;
            bool done = false;
            Enum vEnum;

            theWork = new ArrayList();
            newWork = new ArrayList();
            theResults = new ArrayList();

            while (true)
            {
                memberOf.mainWorkerBarrier.Await();

                if (memberOf.stop) 
                    break;

                // Repeatedly get work, process it, put new work
                while (true)
                {
                    theWork.Clear();
                    newWork.Clear();
                    theResults.Clear();

                    // Attempt to get new work
                    theWork = memberOf.theCoord.Take(memberOf.numItems);

                    // An empty vector is only returned when the computation
                    // has terminated
                    if (theWork.Count == 0) 
                        break;

                    for (int i = 0; i < theWork.Count; i++)
                    {
                        done = ((WorkItemComputation)theWork[i]).DoWork(newWork, theResults);
                        if (done)
                            break;
                    }

                    /*vEnum = theWork.Elements();
                    while (vEnum.HasMoreElements())
                    {
                        done = ((WorkItemComputation)vEnum.NextElement()).
                            doWork(newWork, theResults);
                        if (done) 
                            break;
                    }*/

                    // Short-circuit the computation if indicated
                    if (done)
                    {
                        memberOf.done = true;
                        memberOf.theCoord.NotifyDoneChange();
                    }

                    // Process the results
                    if (theResults.Count != 0)
                    {
                        if (!memberOf.theConfig.IsResultNone())
                        {
                            if (memberOf.theConfig.IsResultExclusive())
                            {
                                memberOf.resultLock.Await();
                            }

                            for (int i = 0; i < theResults.Count; i++)
                            {
                                done = ((ResultItemComputation)theResults[i]).DoResults();
                                if (done)
                                    break;
                            }

                            /*vEnum = theResults.Elements();
                            while (vEnum.HasMoreElements())
                            {
                                done = ((ResultItemComputation)vEnum.NextElement()).DoResults();
                                if (done) 
                                    break;
                            }*/

                            if (memberOf.theConfig.IsResultExclusive())
                            {
                                memberOf.resultLock.Signal();
                            }

                            // Short-circuit the computation if indicated
                            if (done)
                            {
                                memberOf.done = true;
                                memberOf.theCoord.NotifyDoneChange();
                            }

                        }
                        else
                        {
                            // save up the results
                            memberOf.resultPool.Add(theResults);

                        } // end if process some results
                    } // end if any results

                    // Put the new work back
                    memberOf.theCoord.Add(newWork);

                } // end work phase loop

                memberOf.mainWorkerBarrier.Await();
            } // end outer loop
        } // end public void run()
    }
}
