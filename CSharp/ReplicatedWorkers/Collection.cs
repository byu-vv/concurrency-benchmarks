﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReplicatedWorkers
{
    public interface Collection
    {
        int Size();
        Object Take();
        bool Add(Object o);
    }
}
