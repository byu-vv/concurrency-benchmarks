﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReplicatedWorkers
{
    class AQResults : ResultItemComputation
    {
        public static float totalSum;
        public float value;

        public AQResults()
        {
            value = (float)0.0;
        }

        public AQResults(float f)
        {
            value = f;
        }

        public bool DoResults()
        {
            totalSum += value;
            return false;
        }
    }
}
