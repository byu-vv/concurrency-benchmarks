﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ReplicatedWorkers
{
    class FIFOCollection : ArrayList, Collection
    {
        public Object Take()
	    {
		    Object temp;
		    temp = this[0];
            RemoveAt(0);
            return temp;
        }

        public int Size()
        {
            return Count;
        }

        public bool Add(Object o)
        {
            int startSize = Size();
            base.Add(o);
            if (Size() != startSize)
                return true;
            return false;
        }
    }
}
