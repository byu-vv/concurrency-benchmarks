﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Reorder
{
   
    public class ChessTest
    {

        public static void Main(String[] args)
        {
            Console.WriteLine(Run());
        }

        public static Boolean Run()
        {
            
            // This is the shared global variable
            SetCheck sc = new SetCheck();
           
            int iSet = 1;
            int iCheck = 5;

            SetThread[] st = new SetThread[iSet];
            CheckThread[] ct = new CheckThread[iCheck];

            for (int i = 0; i < iSet; i++)
            {
                st[i] = new SetThread(sc);
            }

            for (int i = 0; i < iCheck; i++)
            {
               ct[i] = new CheckThread(sc);
            }

            for (int i = 0; i < iSet; i++)
            {
                st[i].joinTheThread();
            }

            for (int i = 0; i < iCheck; i++)
            {
                ct[i].joinTheThread();
            }
            return true;
        }
    }
}
