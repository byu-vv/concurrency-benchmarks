﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Reorder
{
    public class SetThread
    {
        SetCheck sc;
        Thread setThread;

        public SetThread(SetCheck sc)
        {
            this.sc = sc;
            setThread = new Thread(new ThreadStart(set));
            setThread.Start();
        }

        public void set()
        {
            sc.set();
        }

        public void joinTheThread()
        {
            setThread.Join();
        }
    }
}
