﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reorder
{

    public class SetCheck
    {
        private volatile int a = 0;
        private volatile int b = 0;

        public void set()
        {
                a = 1;
                b = -1;
        }


        public Boolean check()
        {
            return ((a == 0 && b == 0) || (a == 1 && b == -1));
        }
    }
}
