﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Reorder
{
    public class CheckThread
    {
        SetCheck sc;
        Thread checkThread;

        public CheckThread(SetCheck sc)
        {
            this.sc = sc;
            checkThread = new Thread(new ThreadStart(check));
            checkThread.Start();
        }

        public void check()
        {
            Boolean rst = sc.check();
            if (rst != true)
            {
                Console.Write("Bug Found \n");
                Environment.Exit(1);
            }
           
        }


        public void joinTheThread()
        {
            checkThread.Join();
        }

    }
}
